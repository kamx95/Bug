#include "FocusStack.h"


int main()
{
	auto fStack = FocusStack(13, "images/b_bigbug", "_croppped.png");
	fStack.process_images();

	cv::imshow("result", fStack.get_merged());
	cv::imwrite("result.png", fStack.get_merged());
	cv::imshow("depth", fStack.get_depth_map());
	cv::imwrite("depth.png", fStack.get_depth_map());
	cv::waitKey(0);
	return 0;
}