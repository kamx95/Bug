#include "FocusStack.h"

FocusStack::FocusStack(int NUM_IMGS, std::string FILE_PRE, std::string FILE_SUF) :
	NUM_IMGS(NUM_IMGS), FILE_PRE(FILE_PRE), FILE_SUF(FILE_SUF)
{
	char num[5];
	input.resize(NUM_IMGS);
	input_gray.resize(NUM_IMGS);
	for (int i = 0; i < NUM_IMGS; ++i)
	{
		snprintf(num, 5, "%04d", i);
		input[i] = cv::imread(FILE_PRE + num + FILE_SUF, cv::IMREAD_COLOR);
		if (input[i].data == NULL)
		{
			throw std::runtime_error("Can't read image: " + FILE_PRE + num + FILE_SUF);
		}
		color_to_gray(input[i], input_gray[i]);
	}
}

// mirror	->  cb | abc | ba
cv::Point2i FocusStack::border_interpolation(const cv::Mat& im, int x, int y)
{
	if (x < 0)	x = -x;
	if (y < 0)  y = -y;
	if (x > im.cols - 1)
		x = 2 * im.cols - x - 2;
	if (y > im.rows - 1)
		y = 2 * im.rows - y - 2;

	return cv::Point2i(x, y);
}

void FocusStack::color_to_gray(const cv::Mat &color, cv::Mat &gray)
{
	gray = cv::Mat(color.size(), CV_8U);

	for (int i = 0; i < color.rows; ++i)
	{
		auto color_ptr = color.ptr<cv::Vec3b>(i);
		auto gray_ptr = gray.ptr<uchar>(i);
		for (int j = 0; j < color.cols; ++j)
		{
			float luminosity = 0.21f*color_ptr[0][2] + 0.72f*color_ptr[0][1] + 0.07f*color_ptr[0][0];
			*gray_ptr = static_cast<uchar>(std::min(255.f, luminosity));
			++color_ptr;
			++gray_ptr;
		}
	}
}

//adding to output
template<class T>
void FocusStack::sobel_x(const cv::Mat &input_gray, cv::Mat &output)
{
	for (int y = 0; y < input_gray.rows; ++y)
	{
		auto out_ptr = output.ptr<short>(y);
		for (int x = 0; x < input_gray.cols; ++x)
		{
			*out_ptr += input_gray.at<T>(border_interpolation(input_gray, x - 1, y - 1));
			*out_ptr -= input_gray.at<T>(border_interpolation(input_gray, x + 1, y - 1));

			*out_ptr += 2 * input_gray.at<T>(border_interpolation(input_gray, x - 1, y));
			*out_ptr -= 2 * input_gray.at<T>(border_interpolation(input_gray, x + 1, y));

			*out_ptr += input_gray.at<T>(border_interpolation(input_gray, x - 1, y + 1));
			*out_ptr -= input_gray.at<T>(border_interpolation(input_gray, x + 1, y + 1));
			++out_ptr;
		}
	}
}

//adding to output
template<class T>
void FocusStack::sobel_y(const cv::Mat &input_gray, cv::Mat &output) //adding to output
{
	for (int y = 0; y < input_gray.rows; ++y)
	{
		auto out_ptr = output.ptr<short>(y);
		for (int x = 0; x < input_gray.cols; ++x)
		{
			*out_ptr += input_gray.at<T>(border_interpolation(input_gray, x - 1, y - 1));
			*out_ptr += 2 * input_gray.at<T>(border_interpolation(input_gray, x, y - 1));
			*out_ptr += input_gray.at<T>(border_interpolation(input_gray, x + 1, y - 1));

			*out_ptr -= input_gray.at<T>(border_interpolation(input_gray, x - 1, y + 1));
			*out_ptr -= 2 * input_gray.at<T>(border_interpolation(input_gray, x, y + 1));
			*out_ptr -= input_gray.at<T>(border_interpolation(input_gray, x + 1, y + 1));
			++out_ptr;
		}
	}
}

void FocusStack::laplacian(const cv::Mat &input_gray, cv::Mat &output)
{
	cv::Mat tmp(input_gray.size(), CV_16S);
	output = cv::Mat(input_gray.size(), CV_16S);

	tmp = 0;
	output = 0;
	sobel_x<uchar>(input_gray, tmp);
	sobel_x<short>(tmp, output);
	sobel_y<uchar>(input_gray, tmp);
	sobel_y<short>(tmp, output);
}

void FocusStack::variation(const cv::Mat& input, cv::Mat &variation)
{
	variation = cv::Mat(input.size(), CV_32S);

	for (int y = 0; y < input.rows; ++y)
	{
		auto var = variation.ptr<int>(y);
		for (int x = 0; x < input.cols; ++x)
		{
			int avg = 0;
			avg += input.at<short>(border_interpolation(input, x - 1, y - 1));
			avg += input.at<short>(border_interpolation(input, x, y - 1));
			avg += input.at<short>(border_interpolation(input, x + 1, y - 1));

			avg += input.at<short>(border_interpolation(input, x - 1, y));
			avg += input.at<short>(border_interpolation(input, x, y));
			avg += input.at<short>(border_interpolation(input, x + 1, y));

			avg += input.at<short>(border_interpolation(input, x - 1, y + 1));
			avg += input.at<short>(border_interpolation(input, x, y + 1));
			avg += input.at<short>(border_interpolation(input, x + 1, y + 1));

			avg /= 9;

			auto sq = [](short x) { return static_cast<int>(x) * static_cast<int>(x); };

			*var = 0;
			*var += sq(input.at<short>(border_interpolation(input, x - 1, y - 1)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x, y - 1)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x + 1, y - 1)) - avg);

			*var += sq(input.at<short>(border_interpolation(input, x - 1, y)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x, y)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x + 1, y)) - avg);

			*var += sq(input.at<short>(border_interpolation(input, x - 1, y + 1)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x, y + 1)) - avg);
			*var += sq(input.at<short>(border_interpolation(input, x + 1, y + 1)) - avg);

			*var /= 9;
			++var;
		}
	}
}

void FocusStack::merge(const std::vector<cv::Mat>  &var_of_lap)
{
	merged = cv::Mat(input[0].size(), CV_8UC3);
	depth_map = cv::Mat(input[0].size(), CV_8U);
	cv::Mat pos_map = cv::Mat(input[0].size(), CV_8U);

	for (int y = 0; y < input[0].rows; ++y)
	{
		auto pos_map_ptr = pos_map.ptr<uchar>(y);
		for (int x = 0; x < input[0].cols; ++x)
		{
			int max_v = -1;
			int max_pos = -1;
			for (int i = 0; i < NUM_IMGS; ++i)
			{
				if (var_of_lap[i].at<int>(y, x) > max_v)
				{
					max_v = var_of_lap[i].at<int>(y, x);
					max_pos = i;
				}
			}
			*pos_map_ptr = max_pos;
			++pos_map_ptr;
		}
	}
	pos_map = median_filter(pos_map, 5);

	for (int y = 0; y < input[0].rows; ++y)
	{
		auto merged_ptr = merged.ptr<cv::Vec3b>(y);
		auto depth_map_ptr = depth_map.ptr<uchar>(y);
		auto pos_map_ptr = pos_map.ptr<uchar>(y);
		for (int x = 0; x < input[0].cols; ++x)
		{
			*depth_map_ptr = static_cast<uchar>(std::min(1.f - *pos_map_ptr / static_cast<float>(NUM_IMGS - 1) * 255.f, 255.f));
			*merged_ptr = input[*pos_map_ptr].at<cv::Vec3b>(y, x);
			++pos_map_ptr;
			++merged_ptr;
			++depth_map_ptr;
		}
	}
}

void FocusStack::process_images()
{
	std::vector<cv::Mat> var_of_lap(NUM_IMGS);
	int i = 0;
	for (auto &img : input_gray)
	{
		cv::Mat lapl;
		laplacian(img, lapl);
		variation(lapl, var_of_lap[i]);
		++i;
	}
	//merge_winner_takes_all(var_of_lap);
	merge(var_of_lap);
}

cv::Mat FocusStack::median_filter(const cv::Mat &src, int k_size)
//src image is CV_8U
{
	cv::Mat output(src.size(), src.type());
	int idx = 0;
	std::vector<int> window(k_size*k_size);

	for (int y = 0; y < src.rows; ++y)
	{
		auto out_ptr = output.ptr<uchar>(y);
		for (int x = 0; x < src.cols; ++x)
		{
			idx = 0;
			for (int n = y - k_size / 2; n <= y + k_size / 2; ++n)
				for (int m = x - k_size / 2; m <= x + k_size / 2; ++m)
					window[idx++] = src.at<uchar>(border_interpolation(src, m, n));
			std::sort(std::begin(window), std::end(window));
			*out_ptr = window[k_size*k_size / 2];
			++out_ptr;
		}
	}
	return output;
}