#pragma once

#include "opencv2/opencv.hpp"
class FocusStack
{
private:
	const int NUM_IMGS;
	const std::string FILE_PRE, FILE_SUF;
	std::vector<cv::Mat> input, input_gray;
	cv::Mat merged, depth_map;

public:
	FocusStack(int NUM_IMGS, std::string FILE_PRE, std::string FILE_SUF);
	void process_images();
	inline const cv::Mat& get_merged() const {return merged; };
	inline const cv::Mat& get_depth_map() const { return depth_map; };
private:
	cv::Point2i border_interpolation(const cv::Mat &im, int x, int y);
	void color_to_gray(const cv::Mat &color, cv::Mat &gray);
	template<class T> void sobel_x(const cv::Mat &input_gray, cv::Mat &output); 
	template<class T> void sobel_y(const cv::Mat &input_gray, cv::Mat &output);
	void laplacian(const cv::Mat &input_gray, cv::Mat &output);
	void variation(const cv::Mat &input, cv::Mat &variation);
	void merge(const std::vector<cv::Mat>  &var_of_lap);
	cv::Mat median_filter(const cv::Mat &src, int k_size);
};